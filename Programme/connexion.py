
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
# Connect to database radio-libre with Postgresql
import psycopg2
from . import interface


def connect_to_database(request):
    """Manage the connection to the database 'radio-libre'
    Keyword Arguments:
    request -- request recovered in the where clause generator
    """
    try:
        connect_str = "dbname='radio_libre' user='s.panthier' host='postgresql.bts-malraux72.net' password='P@ssword'"
        conn = psycopg2.connect(connect_str)
        cursor = conn.cursor()
        interface.logging.info('Connect to database sucess')
    except psycopg2.DatabaseError as error:
        interface.logging.critical(error)
        exit()
    try:
        cursor.execute(request)
        rows = cursor.fetchall()
        if len(rows) > 0:
            interface.logging.info('%s parts were recovered' % len(rows))
        else:
            interface.logging.debug('No parts was recovered')
        cursor.close()
        conn.close()
        return rows
    except psycopg2.DatabaseError as error:
        interface.logging.critical(error)
        exit()
