
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
import random
from . import interface
import os
def gestion_playliste(the_playlist, arguments, critere):
    """Playlists Management :
    Function that manage the playlists on criterias of duration

    Keyword Arguments:
    the_playlist -- the list of parts
    argument --  type of seach in the playlist
    critere -- name of attribut of playlist
    """
    out = []
    duree_max = 0
    console_info = ""
    temp = 0
    if critere in ['genre', 'artiste', 'album', 'titre']:
        if critere == "title":
            critere = "titre"
        if critere == "artist":
            critere = "artiste"
        if len(the_playlist) > 0:
            for argument in arguments:
                try:
                    duration_in_minutes = int(argument[1])
                    if duration_in_minutes > 4 and duration_in_minutes < 101:
                        duree_max += duration_in_minutes * 60
                        console_info = "The total of playlist wanted is % seconds." % duree_max
                    else:
                        error_message = "Out of range. Duration must be between 5 and 100"
                        interface.logging.critical(str(error_message))
                        exit()
                    marge = 30
                    duree_morceau = 0
                    for morceau in the_playlist:
                        duree_morceau += int(morceau[1])
                        if duree_morceau <= (duree_max-marge):
                            out.append(str(morceau[2]))
                            temp = duree_morceau
                except TypeError as error:
                    interface.logging.critical("ERROR : %s " % error)
                    interface.logging.info("Required an integer")
                    exit()
        random.shuffle(out)
        console_debug = "The total duration of the playlist is %s parts." % len(out)
        console_info += " The total of the final playlist % seconds." % temp
        interface.logging.debug(str(console_debug))
        interface.logging.info(str(console_info))
        return out
    else:
        interface.logging.critical("the criteria %s is incorrect" % critere)
