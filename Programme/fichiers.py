
"""
    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from . import interface
import os

def write_m3u(the_playlist, filename):
    """Write parts to a M3U file

    Keyword Arguments:
    filename -- file name
    the_playlist -- list of paths to write in the file
    """
    if len(the_playlist) > 0:
        try:
            if not os.path.exists('playlists'):
                os.mkdir('playlists')
            extension = '.M3U'
            filename += extension
            path_to_file = "playlists/%s" % filename
            my_file = open(path_to_file, "w")
            for song in the_playlist:
                string = str(song)
                my_file.write(string + "\n")
            my_file.close()
            error_message = 'The playlist %s is save ' % path_to_file
            error_message += 'and contains %s parts' % len(the_playlist)
            interface.user.info(str(error_message))
        except IOError:
            error_message = "The file %s has occured an error" % filename
            interface.user.error(str(error_message))
            verbosity_error_message = "file not found: {}".format(filename)
            interface.user.critical(str(verbosity_error_message))
def write_txt(the_playlist, filename):
    """Write parts to a TXT file

    Keyword Arguments:
    filename -- file name
    the_playlist -- list of paths to write in the file
    """
    if len(the_playlist) > 0:
        try:
            if not os.path.exists('playlists'):
                os.mkdir('playlists')     
            extension = '.txt'
            filename += extension
            path_to_file = "playlists/%s" %  filename
            my_file = open(path_to_file, "w")
            for song in the_playlist:
                string = str(song)
                my_file.write(string + "\n")
            my_file.close()
            error_message = 'The playlist %s is save ' % path_to_file
            error_message += 'and contains %s parts' % len(the_playlist)
            interface.user.info(str(error_message))
        except IOError:
            error_message = "The file %s has occured an error" % filename
            interface.user.error(str(error_message))
            verbosity_error_message = "file not found: {}".format(filename)
            interface.user.critical(str(verbosity_error_message))
def write_xspf(the_playlist, filename):
    """Write elements to a XSPF file

    Keyword Arguments:
    filename -- file name
    the_playlist -- list of paths to write in the file
    """
    #<?xml version="1.0" encoding="UTF-8"?>
    try:
        if not os.path.exists('playlists'):
            os.mkdir('playlists')
        extension = '.xspf'
        filename += extension
        path_to_file = "playlists/%s" %  filename
        playlist = Element("playlist")
        playlist.set("version", "1")
        playlist.set("xmlns", "http://xspf.org/ns/0/")
        track_list = SubElement(playlist, "trackList")
        for line in the_playlist:
            track = SubElement(track_list, "track")
            SubElement(track, "location").text = line
        tree = ElementTree(playlist)
        tree.write(path_to_file, xml_declaration="""<?xml version="1.0" encoding="UTF-8"?>""")
        error_message = 'The playlist %s is save ' % path_to_file
        error_message += 'and contains %s parts' % len(the_playlist)
        interface.user.info(str(error_message))
    except IOError:
        error_message = "The file %s has occured an error" % filename
        interface.user.error(str(error_message))
        verbosity_error_message = "file not found: {}".format(filename)
        interface.user.critical(str(verbosity_error_message))



