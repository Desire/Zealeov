# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017

@author: PANTHIER Sharon & PATUREAU Désiré

    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
def expression(word):
    """Convert the string with the ASCII Table

    Keyword Arguments:
    word -- word to convert
    """
    if word != '^$' and word != '^ $':
        sub_string = word[0]
        change_to_ascii = ord(sub_string)
        rest = ''
        letter = ""
        for i in range(1, len(word)):
            rest += word[i]
        if change_to_ascii >= 97 and change_to_ascii <= 123:
            letter = "[%s%s]%s" % (chr(change_to_ascii-32), sub_string, rest)
        elif change_to_ascii >= 65 and change_to_ascii <= 90:
            letter = "[%s%s]%s" % (sub_string, chr(change_to_ascii+32), rest)
        else:
            letter = word
        return letter
    else:
        letter = word
        return letter
        
