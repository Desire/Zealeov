#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
import argparse
from . import interface
from .where_generator import generator
from .convertisseur import gestion_playliste
from .fichiers import write_m3u
from .fichiers import write_txt
from .fichiers import write_xspf


cli = argparse.ArgumentParser("Playlist Generator : Zealeov")
cli.add_argument(
    '-V', '--version',
    action="version",
    version="%(prog)s 1.1")
cli.add_argument(
    'file',
    help="name of playlist",
    metavar='FILE')
cli.add_argument(
    '-v', '--verbose',
    help='Increase the language verbosity',
    action="count",
    default=0)
cli.add_argument(
    '-d', '--debug',
    help='switch to debug mode',
    action='store_true',
    default=0)
cli.add_argument(
    '--genre',
    help='retrieves the songs by genre passed in arguments',
    nargs=2,
    action='append')
cli.add_argument(
    '--artist',
    help='retrieves the songs by artist passed in arguments',
    nargs=2,
    action='append')
cli.add_argument(
    '--album',
    help='retrieves the songs by album passed in arguments',
    nargs=2,
    action='append')
cli.add_argument(
    '--title',
    help='retrieves the songs by title passed in arguments',
    nargs=2,
    metavar=('TITLE', int),
    action='append')
cli.add_argument(
    '--output',
    help='output type of the playlist',
    type=str)
# more or less verbose display depending on the parameter -v
def set_log_level_from_verbose(cli_args):
    """Set log level from verbose or
    if the user decide to switch in debug mode.

    Keyword Arguments:
    cli_args -- user input
    """
    if not cli_args.verbose:
        interface.console_handler.setLevel('CRITICAL')
    elif cli_args.verbose == 1:
        interface.console_handler.setLevel('ERROR')
    elif cli_args.verbose == 2:
        interface.console_handler.setLevel('INFO')
    elif cli_args.verbose >= 3:
        interface.console_handler.setLevel('DEBUG')
    else:
        interface.user.critical("UNEXPLAINED NEGATIVE COUNT!")
    if cli_args.debug:
        interface.console_handler.setLevel('DEBUG')
def args_management(cli_args):
    """Manage all arguments user's input

    Keyword Arguments:
    cli_args -- user input
    """
    ma_play = []
    argument = ""
    sigle = [['^$', 0]]
    for critere in ['genre', 'album', 'title', 'artist']:
        argument = getattr(cli_args, critere, None)
        if critere == 'album':
            sigle = [['^ $', 0]]
        else:
            sigle = [['^$', 0]]
        if argument is None:
            argument = sigle
        if critere == "title":
            critere = "titre"
        if critere == "artist":
            critere = "artiste"
        try:
            playliste = generator(argument, critere)
            ma_play += gestion_playliste(playliste, argument, critere)
        except Exception as error:
            interface.logging.critical(error)
        if not cli_args.output:
            write_txt(ma_play, cli_args.file)
        elif cli_args.output == "M3U" or cli_args.output == "m3u":
            write_m3u(ma_play, cli_args.file)
        elif cli_args.output == "XSPF" or cli_args.output == "xspf":
            write_xspf(ma_play, cli_args.file)
    print("The playlist contain %s parts." % len(ma_play))
def start():
    args = cli.parse_args()
    set_log_level_from_verbose(args)
    args_management(args)

