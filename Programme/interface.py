# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 11:04:20 2017

    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
"""

import logging
from logging.handlers import TimedRotatingFileHandler
import os
# We configure the logs
user = logging.getLogger()
user.setLevel(logging.DEBUG)
# Redirect all logs in the file name 'Zealeov.log'
if not os.path.exists('logs'):
    os.mkdir('logs')
log_file_handler = TimedRotatingFileHandler('logs/zealeov.log', when='M', interval=2)
formatter = '%(asctime)s [%(levelname)s](%(funcName)s:%(lineno)d): %(message)s'
log_file_handler.setFormatter(logging.Formatter(formatter))
log_file_handler.setLevel(logging.ERROR)
user.addHandler(log_file_handler)

# Redirect all logs to the console
console_handler = logging.StreamHandler() # sys.stderr
console_handler.setLevel(logging.ERROR)
console_handler.setFormatter(logging.Formatter(formatter))
user.addHandler(console_handler)





