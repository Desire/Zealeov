"""
    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
    Générateur de clause where sur la liste des critères et les arguments
"""
from .function import expression
from .connexion import connect_to_database
from . import interface
def generator(arguments, criteria, add_request=" order by random();"):
    """generator of where clause in sql

    Keyword Arguments:
    arguments -- name of argument to search in database
    criteria -- criteria of attribute
    add_request -- end of sql request can be change
    """
    start_sql_request = "select genre, duree, chemin from morceaux where "
    where = ""
    sql = ""
    if arguments is not '^$' or not '^ $':
        cpt_element = 0
        for element in arguments:
            element = expression(element[0])
            cpt_element = cpt_element + 1
            if cpt_element <= 1:
                where = "%s ~ '%s'" % (criteria, element)
            else:
                where += " or %s ~ '%s'" % (criteria, element)
            sql = start_sql_request + where + add_request
            interface.logging.debug(sql)
    else:
        interface.logging.error('No arguments for this section')
    search_in_database = connect_to_database(sql)
    return search_in_database
