# Zealeov 
[![python](https://img.shields.io/badge/python-3.5-green.svg)](https://docs.python.org/3/)
[![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://www.gnu.org/licenses/licenses.fr.html)

## What's Zealeov
Zealeov is a playlists generator that allow users to generate automatically a new playlist by means preset by themself.

## Instruction
The main instruction for use Zealeov is : 

    generateur FILE [--genre] [--artist] [--album] [--output] [--debug] [--verbosity]

## Installation

### Prerequisites
You'll need one thing to install Zealeov:  
[![python](https://img.shields.io/badge/python-3.5-green.svg)](https://docs.python.org/3/)

### Local install

If you want to set up the software in its own directory, you can clone
the git repository, so:

    git clone https://framagit.org/sio-malraux/Zealeov.git
    
### Source package
Make sure you get the setup tool by the command line: 

    apt install python3-setuptools

How to install package : 

    pip3 install Zealeov-x.x.x

How to uninstall package : 

    pip3 uninstall --yes Zealeov==x.x.x
    
*Only administrator can install/uninstall package*
### Debian package
How to install package : 

    dpkg -i python3-programme_x.x.x_all.deb

How to uninstall package : 

    apt remove python3-programme
    
*Only administrator can install/uninstall package*    
## License

[![GNU AGPLv3 Image](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl-3.0.html) 

**Zealeov** is [Free Software][freesoftware]: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
**GNU AGPL 3.0 License** as published by the Free Software Foundation.

See the [COPYING][LICENSE] file for the full license text


 [LICENSE]: https://framagit.org/sio-malraux/Zealeov/blob/master/Documentation/LICENCES/LICENCES-EN
 [freesoftware]: https://www.gnu.org/philosophy/free-sw.html
