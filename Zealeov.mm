<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Zealeov" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1504709301845"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="1"/>
<node TEXT="Code" POSITION="right" ID="ID_58013208" CREATED="1504706301831" MODIFIED="1504709301844" HGAP="26" VSHIFT="10">
<edge COLOR="#00ff00"/>
<node TEXT="Moteur" ID="ID_868362253" CREATED="1504706368101" MODIFIED="1504706724584" HGAP="40" VSHIFT="-20">
<node TEXT="Sortie" ID="ID_1265983142" CREATED="1504706901570" MODIFIED="1504706905167">
<node TEXT="M3U" ID="ID_1418111214" CREATED="1504706944882" MODIFIED="1504706951143"/>
<node TEXT="XSPF" ID="ID_136946604" CREATED="1504706951898" MODIFIED="1504706956951"/>
</node>
<node TEXT="s&#xe9;lection des morceaux adapt&#xe9;s aux crit&#xe8;res fournis en CLI" ID="ID_930212367" CREATED="1504708286536" MODIFIED="1504708313060">
<node TEXT="connexion BDD" ID="ID_1398880302" CREATED="1504708809028" MODIFIED="1504708816097"/>
<node TEXT="SELECT FROM WHERE;" ID="ID_1641179369" CREATED="1504708817532" MODIFIED="1504708830913"/>
<node TEXT="trier" ID="ID_1046587634" CREATED="1504708847316" MODIFIED="1504708850745"/>
<node TEXT="close BDD" ID="ID_61619191" CREATED="1504708831900" MODIFIED="1504708836857"/>
</node>
<node TEXT="journalisation" ID="ID_1886288289" CREATED="1504708757501" MODIFIED="1504708761009"/>
</node>
<node TEXT="CLI : interface en ligne de commande" ID="ID_314819528" CREATED="1504706389366" MODIFIED="1504708079231">
<node TEXT="crit&#xe8;res de s&#xe9;lection" ID="ID_293284021" CREATED="1504708198929" MODIFIED="1504708215650"/>
<node TEXT="crit&#xe8;re de dur&#xe9;e" ID="ID_1769294192" CREATED="1504708218025" MODIFIED="1504708241021"/>
<node TEXT="s&#xe9;lection al&#xe9;atoire ou tri&#xe9;e" ID="ID_1130002000" CREATED="1504708916708" MODIFIED="1504708939232"/>
<node TEXT="type de sortie" ID="ID_358521868" CREATED="1504708242784" MODIFIED="1504708262069"/>
</node>
</node>
<node TEXT="Base de donn&#xe9;es" POSITION="right" ID="ID_1251480819" CREATED="1504706381294" MODIFIED="1504709299164" HGAP="11" VSHIFT="-65">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="documentations" POSITION="left" ID="ID_1631150574" CREATED="1504709210745" MODIFIED="1504709293885">
<edge COLOR="#ff0000"/>
<node TEXT="Technique" ID="ID_689988171" CREATED="1504706263875" MODIFIED="1504709328701" HGAP="70" VSHIFT="30"/>
<node TEXT="Utilisateur" LOCALIZED_STYLE_REF="default" ID="ID_900127827" CREATED="1504706216837" MODIFIED="1504709325262" HGAP="36" VSHIFT="9">
<node TEXT="CopyRight" ID="ID_1946194942" CREATED="1504707031241" MODIFIED="1504709229383" HGAP="26" VSHIFT="1"/>
<node TEXT="HTML" ID="ID_1407919836" CREATED="1504707089441" MODIFIED="1504709248276" HGAP="31" VSHIFT="29"/>
<node TEXT="LaTex" ID="ID_1296602357" CREATED="1504707082361" MODIFIED="1504709257681" HGAP="25" VSHIFT="-33"/>
<node TEXT="PDF" ID="ID_520974620" CREATED="1504707094065" MODIFIED="1504709262964" HGAP="33" VSHIFT="31"/>
</node>
<node TEXT="Internationalisation" ID="ID_493722496" CREATED="1504706826163" MODIFIED="1504709355114" VSHIFT="49"/>
</node>
</node>
</map>
