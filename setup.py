from setuptools import setup
setup(
    author="Panthier Sharon and Patureau Désiré",
    author_email="sharon-joyce.panthierfantin@bts-malraux.net",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: End User/Desktop',
        'License :: OSI Approved :: GNU General Public License c3 or later (GPLv3+)'
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: Multimedia :: Sound/Audio',
        'Topic :: Utilities'],
    description="Playlist Generator",
    entry_points={
        'console_scripts': [
            'zealeov = Programme:start'
        ]
    },
    install_requires=["psycopg2"],
    keywords='radio playlist generator',
    license="GPLv3+",
    long_description="Generate playlists automatically",
    data_files=[
                   ('/usr/share/man/man1',['Programme/Documentation/User_Manual/zealeov.1.gz']),
                   ('/usr/share/doc/zealeov', ['LICENCE',
                                                'Programme/Documentation/User_Manual/zealeov.html',
                                                'Programme/Documentation/User_Manual/zealeov.pdf'])
               ],    
    name="Zealeov",
    packages=["Programme"],
    #python_requires='>=3.2, <4',
    url="",
    version="1.0"
)

